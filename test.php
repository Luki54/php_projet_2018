<?php
/**
 * Created by PhpStorm.
 * User: gabrielsega
 * Date: 21/11/2018
 * Time: 09:14
 */

require_once('vendor/autoload.php');
use wishlist\modele\Item as Item;
use wishlist\modele\Liste as Liste;
use Illuminate\Database\Capsule\Manager as DB;

//on initialise eloquent
$ini = parse_ini_file('src/conf/conf.ini');
$db = new DB();
$db->addConnection($ini);
$db->setAsGlobal();
$db->bootEloquent();

//affichage des listes
$listes = Liste::select('no', 'titre')->get();

foreach($listes as $l){
    echo $l->no . " " . $l->titre . "<br>";
}

//affichage des items
echo "<br>";
$items = Item::select('id','nom', "liste_id")->get();

foreach($items as $item){
    echo $item->id . " " . $item->nom . " et la liste : " ;
    if(isset($item->liste->titre)){
        echo $item->liste->titre . "<br>";
    }
}

//affichage d'un item en particulier
echo "<br>";
if(isset($_GET["id"])){
    $objet = Item::select('id', 'nom')->where("id", "=", $_GET["id"])->get()->first();
    echo "Objet demandé : " . $objet->nom . "<br>";
}

//insertion d'un item
$i = new Item();
$i->nom = "Jouet";
$i->liste_id = "1";
//$i->save();


//pour lister la liste demandée
if(isset($_GET["liste"])){
    //on récupère déjà la liste
    $li = Liste::select('*')->where("no", "=", $_GET["liste"])->get()->first;

    //on récupère les objets
    $objs = $li->item;

    echo $objs;
}
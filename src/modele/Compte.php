<?php
/**
 * Created by PhpStorm.
 * User: gabrielsega
 * Date: 08/01/2019
 * Time: 11:17
 */

namespace wishlist\modele;


class Compte extends \Illuminate\Database\Eloquent\Model {
    //eloquent
    protected $table = 'compte';
    protected $primaryKey = 'idCompte';
    public $timestamps = false;

    function aParticipe(){
        if($this->type == "participant"){
            return $this->hasMany("wishlist\modele\Reservation", "id_participant");
        }
    }
}
<?php

namespace wishlist\modele;

class Message extends \Illuminate\Database\Eloquent\Model {
	//eloquent
	protected $table = 'message';
	protected $primaryKey = 'idMessage';
	public $timestamps = false;

    public function liste(){
        return $this->belongsTo("wishlist\modele\Liste", "liste_id");
    }

    public function item(){
        return $this->belongsTo("wishlist\modele\Item", "item_id");
    }
}
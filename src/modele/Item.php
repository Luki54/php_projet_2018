<?php
/**
 * Created by PhpStorm.
 * User: gabrielsega
 * Date: 21/11/2018
 * Time: 09:03
 */

namespace wishlist\modele;

class Item extends \Illuminate\Database\Eloquent\Model {
    //eloquent
    protected $table = 'item';
    protected $primaryKey = 'id';
    public $timestamps = false;

    public function liste(){
        return $this->belongsTo("wishlist\modele\Liste", "liste_id");
    }
}
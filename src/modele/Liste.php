<?php
/**
 * Created by PhpStorm.
 * User: gabrielsega
 * Date: 21/11/2018
 * Time: 09:10
 */

namespace wishlist\modele;

class Liste extends \Illuminate\Database\Eloquent\Model{
    //eloquent
    protected $table = 'liste';
    protected $primaryKey = 'no';
    public $timestamps = false;

    public function item(){
        return $this->hasMany("wishlist\modele\Item", 'liste_id');
    }
    
    public function message(){
    	return $this->hasMany("wishlist\modele\Message", 'liste_id');
    }

    public function createur(){
        return $this->belongsTo("wishlist\modele\Compte", "user_id");
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: gabrielsega
 * Date: 28/12/2018
 * Time: 12:37
 */

namespace wishlist\modele;


class Reservation extends \Illuminate\Database\Eloquent\Model {
    //on redéfinit les propriétés
    protected $table = "reservation";
    protected $primaryKey = "idReservation";
    public $timestamps = false;

    //fonctions
    public function item(){
        return $this->belongsTo("wishlist\modele\Item", "item_id");
    }
}
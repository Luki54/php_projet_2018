<?php
/**
 * Created by PhpStorm.
 * User: jean-baptistebenard
 * Date: 2019-01-15
 * Time: 15:22
 */

namespace wishlist\modele;

class Cagnotte extends \Illuminate\Database\Eloquent\Model {
    //eloquent
    protected $table = 'cagnotte';
    protected $primaryKey = 'idCagnotte';
    public $timestamps = false;

    public function item(){
        return $this->belongsTo("wishlist\modele\Item", "id");
    }
}
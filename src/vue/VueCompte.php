<?php
/**
 * Created by PhpStorm.
 * User: gabrielsega
 * Date: 08/01/2019
 * Time: 11:04
 */


namespace wishlist\vue;
use wishlist\modele\Liste;
use wishlist\modele\Item;

class VueCompte {
    private $objets;

    public function __construct($obj) {
        $this->objets = $obj;
    }

    private function afficherFormulaireCreation($mode){
        //on récupère l'application
        $app = \Slim\Slim::getInstance();

        if($mode == null){
            $action = "creationCompte";
        }else{
            $action = $mode;
        }

        //l'url de connexion
        $url = $app->urlFor("creationCompte");
        //pour la modification
        $modif = $app->urlFor("afficherModif");

        if(isset($_SESSION["login"])){
            $nom = $_SESSION["login"];
            $html = <<<END
            <h1> Bonjour $nom </h1>
            <form id="f6" method="post" action="">
                <button type="submit" class="btn btn-primary mb-2" name="$action" value="">Déconnexion</button> 
            </form>
            <a class="btn btn-primary" href="$modif">Modifier compte</a>
END;

        }else{
            //on vérifie qu'il s'agit d'une création de compte
            if($action == "creationCompte"){
                $form = <<<END
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="exampleRadios" id="createur" value="createur" checked>
                    <label class="form-check-label" for="createur">
                        Créateur
                    </label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="exampleRadios" id="participant" value="participant">
                    <label class="form-check-label" for="participant">
                        Participant
                    </label>
                </div>
END;
            }else{
                $form = "";
            }

            $html = <<<END
            <form id="f5" method="post" action="" class="pb-4">
                <input type="text" placeholder="Pseudo" name="pseudo"> 
                <input type="password" placeholder="Mot de passe" name="mdp">
                $form
                <button type="submit" class="btn btn-primary mb-2" name="$action" value="">Valider</button> 
            </form>
END;


            //si on est sur la page de connexion
            if($action=="connexion"){
                $html = $html . "<form id=\"f6\" method=\"get\" action=\"$url\" class='pb-4'>
                                    <button type=\"submit\" class=\"btn btn-primary mb-2\" name=\"\" value=\"\">Créer un compte</button>
                                </form>";
            }
        }

        //pour le titre
        $titre = ucfirst($action);

        //on définit avec CSS
        $html = <<<END
            <div class="py-5 bg-light">
                    <div class="container ">
                        <div class="row bg-white border rounded">
                                <div class="col-lg-12">
                                    <h1 class="text-center pb-3">$titre</h1>
                                    $html
                                </div>
                                
                         </div>
                    </div>
            </div>
END;

        return $html;
    }

    //pour modifier le compte
    private function afficherFormulaireModification(){
        //on récupère les valeurs
        $pseudo = $_SESSION["login"];

        //le formulaire qu'on va afficher
        $html = <<<END
         <form id="f9" method="post" action="">
            <input type="text" readonly placeholder="Pseudo" name="pseudo" value = "$pseudo"> 
            <input type="password" placeholder="Ancien Mot de passe" name="ex-mdp">
            <input type="password" placeholder="Nouveau Mot de passe" name="mot">
            <button type="submit" class="btn btn-primary mb-2" name="modifierCompte" value="">Valider</button> 
            <button type="submit" class="btn btn-primary mb-2" name="supprimerCompte" value="">Supprimer le compte</button> 
         </form>
         
END;

        $html = <<<END
        <div class="py-5 bg-light">
                    <div class="container ">
                        <div class="row bg-white border rounded">
                                <div class="col-lg-12">
                                    <h1 class="text-center pb-3">Modifier le compte</h1>
                                    $html
                                </div>
                                
                         </div>
                    </div>
            </div>
END;

        return $html;
    }

    //pour un problème de mot de passe
    private function mdpIncorrect(){
        return "erreur avec le mot de passe";
    }

    //pour un accès fait par le mauvais chemin
    private function pasConnecte(){
        return "Vous n'êtes pas passé par le bon chemin";
    }

    public function render($select){
        switch ($select){
            case -2:
                $content = $this->pasConnecte();
                break;
            case -1:
                $content = $this->mdpIncorrect();
                break;
            case 1:
                $content = $this->afficherFormulaireCreation(null);
                break;
            case 2:
                $content = $this->afficherFormulaireCreation("connexion");
                break;
            case 3:
                $content = $this->afficherFormulaireCreation("deconnexion");
                break;
            case 4:
                $content = $this->afficherFormulaireModification();
                break;
        }

        //l'url pour les headers
        $app = \Slim\Slim::getInstance();
        $urlCre = $app->urlFor("creationListe");
        $urlParticipation = $app->urlFor("participations");
        $urlConnexion = $app->urlFor("connexion");
        $urlAccueil = $app->urlFor("accueil");
        $urlChercher = $app->urlFor("rechercher");
        $urlCreerItem = $app->urlFor("creerItem");
        $urlCreateurs = $app->urlFor("createurs");
        $urlJoin = $app->urlFor("joindreUneListe");

        //si il est connecté
        if(isset($_SESSION["login"])){
            $nom = $_SESSION["login"];

            //on affiche son nom
            $accroche = <<<END
            <li class="nav-item">
                <a class="nav-link" href="$urlConnexion">Bienvenue $nom </a>
            </li>
END;

            if($_SESSION["type"] == "participant"){
                $particularite = <<<END
                <li class="nav-item">
                    <a class="nav-link" href="$urlParticipation"> Mes participations</a>
                </li>
END;
            }else {
                $particularite = <<<END
                <li class="nav-item">
                    <a class="nav-link" href="$urlCre"> Créer une liste</a>
                </li>
END;
            }
        }else{
            $accroche = "";
            $particularite = <<<END
                <li class="nav-item">
                    <a class="nav-link" href="$urlCre"> Créer une liste</a>
                </li>
END;
        }


        //on remplit le html
        $html = <<<END
        <!DOCTYPE html>
        <html>
            <head>
                <meta charset="UTF-8">
                <meta http-equiv="X-UA-Compatible" content="IE=edge">
                <meta name="viewport" content="width=device-width, initial-scale=1">
        
                <link href="bootstrap/css/bootstrap.css" rel="stylesheet">
                <link href="bootstrap/css/perso.css" rel="stylesheet">
            </head>
        <body>
        
        <header>
            <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
                <a class="navbar-brand" href="$urlAccueil">Wishlist</a>
                <div class="collapse navbar-collapse">
                    <ul class="navbar-nav col-md-12">
                        $particularite
                        <li class="nav-item">
                            <a class="nav-link" href="$urlConnexion"> Se connecter</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="$urlCreerItem"> Créer un item</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="$urlCreateurs"> Créateurs</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="$urlJoin"> Joindre une liste</a>
                        </li>
                        $accroche
                        <form class="form-inline my-1 my-lg-0" action="$urlChercher">
                            <button class="btn btn-outline-info my-1 my-sm-0 " type="submit">Rechercher</button>
                        </form>
                    </ul>
                </div>
            </nav>
        </header>

            $content
            
        </body>
        </html>

END;

        echo $html;
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: gabrielsega
 * Date: 28/12/2018
 * Time: 19:25
 */

namespace wishlist\vue;
use Slim\Slim;
use wishlist\modele\Liste;
use wishlist\modele\Item;
use wishlist\modele\Reservation;

class VueCreateur {
    private $objets;

    public function __construct($obj) {
        $this->objets = $obj;
    }

    private function listeDetaille(){
        //pour accéder à l'application
        $app = \Slim\Slim::getInstance();

        //on ajoute les infos de la liste
        $l = $this->objets['liste'];
        $titre = $l["titre"];
        $desc = $l['description'];
        $exp = $l['expiration'];
        $phrase = $desc . ", vous avez jusqu'au $exp pour participer à cette liste";

        //la cible du bouton de modification et partage
        $url = $app->urlFor("modifierListe", ['tokenModif'=>$l["tokenModif"], 'numListe' =>$l["no"]]);
        $urlPartage = $app->urlFor("partager", ['numListe' =>$l["no"]]);
        
        $deb = <<<END
        <section class="jumbotron text-center">
            <div class="container">
                <h1>$titre</h1>
                <p class="lead text-muted"> $phrase</p>
                <form id='f2' method='get' action=$url>
                    <button type="submit" class="btn btn-primary mb2">Modifier la liste</button>
                </form>
				<br>
				<form id='f2' method='post' target="_blank" action=$urlPartage>
                    <button type="submit" class="btn btn-primary mb2">Obtenir l'URL de partage</button>
                </form>

            </div>
        </section>
END;

        $ch = $deb . "\n" . "<div class='album py-5 bg-light'> \n <div class='container'> \n <div class='row'>";

        // on ajoute les items de la liste
        foreach ($this->objets['item'] as $obj){
            //url de redirection
            $url = $app->urlFor("item", ['id'=>$obj['id'], 'token' => $this->objets['token']]);

            //pour l'url de son image
            if($obj["urlImage"] == 1){
                $src = $obj["img"];
            }else{
                $src = "img/" . $obj['img'];
            }

            //pour ses infos
            $nom = $obj["nom"];
            $desc = $obj["descr"];
            $tarif = $obj['tarif'];

            //pour son état de réservation
            if($obj['reservation'] == 0){
                $res = "pas réservé";
            }else{
                $res ="réservé";
            }

            $text = $desc . ", " . $tarif . ", " . $res;

            $ob = <<<END
            <div class="col-md-3 card-group">
                <div class="card ">
                    <img class="card-img-top img-fluid" src="$src">
                    <div class="card-body " >
                        <h5 class="card-title">$nom</h5> 
                        <p class="card-text "> $text </p>
                    </div>
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item text-center">                                            
                            <a class="btn btn-primary" href="$url">Voir les détails</a>
                        </li>
                    </ul>
                </div>
            </div>
END;
            $ch = $ch . "\n" . $ob . "\n";



        }



        //on rajoute pour rendre public
        if($l->public == 0){
            //pour mettre en publique
            $url = $app->urlFor("rendrePublique", ["id" => $l->no]);

            $public = "<li class=\"list-group-item text-center\"> <form name='rendrePublic' method='post' action='$url'><button type=\"submit\" class=\"btn btn-primary mb2\" name='public'>Rendre publique</button></form></li>";
        }else{
            $public = "";
        }

        //on rajoute pour la validation
        if($l->validee == 0){
            //pour valider
            $url = $app->urlFor("rendreValide", ["id" => $l->no]);

            $validee = "<li class=\"list-group-item text-center\"> <form name='rendreValide' method='post' action='$url'><button type=\"submit\" class=\"btn btn-primary mb2\" name='valide'>Valider la liste</button></form></li>";
        }else{
            $validee = "";
        }


        $urlAjout = $app->urlFor("afficherAjoutItem", ["idListe"=>$l->no]);
        //la carte des options
        $carte = <<<END
            <div class="col-md-3 ">
                <div class="card ">
                    <div class="card-body " >
                        <h5 class="card-title">Options</h5> 
                    </div>
                    <ul class="list-group list-group-flush">
                            $public
                            $validee     
                            <li class="list-group-item text-center"> 
                                <a href="$urlAjout" class="btn btn-primary">Ajouter un item</a>
                            </li>
                                
                        </li>
                    </ul>
                </div>
            </div>
END;


        //on rajoute à la grande chaine
        $ch = $ch . $carte;



        //on regarde vis à vis de la date d'expisration
        if(strtotime($l["expiration"]) < time()){
            //on ajoute tous les messages
            foreach ($this->objets['message'] as $message){
                $mes= $message["message"];

                $ch = $ch . <<<END
                <div class="col-md-3 ">
                    <div class="card ">
                        <div class="card-body " >
                            <h5 class="card-title">Message</h5> 
                            <p class="card-text "> $mes </p>
                        </div>
                    </div>
                </div>
END;


            }
        }

        $ch = $ch . "</div>" . "\n" . "</div>" . "\n" . "</div>";




        return $ch;
    }

    //pour créer ou modifier une liste
    private function afficherFormulaireListe(){
        //on récupère la liste
        if($this->objets == null){
            $liste = new Liste();
            $valeur = "creationListe";
        }else{
            $liste = $this->objets["liste"];
            $valeur = "modificationListe";
        }

        //on récupère les valeurs
        $titre = $liste["titre"];
        $des = $liste["description"];
        $exp = $liste["expiration"];

        //on peut définir action avec url for
        $code = <<<END
        <div class="py-5 bg-light">
            <div class="container ">
                <div class="row bg-white border rounded">
                    <div class="col-md-12">
                        <form id="f1" method="post" action="">
                            <div class="row">
                                <label for="titre" class="col-sm-4 col-form-label">Titre de la liste : </label>
                                <div class="col-lg-8">
                                    <input type="text" name="titre" class="form-control form-control-sm" id="titre" placeholder="titre" value="$titre">
                                </div>
                                <div class="form-group col-md-12">
                                    <label for = "description">Description de la liste : </label>
                                    <textarea name = "description" class="form-control col-md-12" id="description" rows="3"> $des </textarea>
                                </div>
                                <div class="form-group col-md-12">
                                    <label for = "date">Date d'expiration </label>
                                    <input type="date" name="date" placeholder="JJ/MM/AAAA" class="form-control" value="$exp"> 
                                </div>
                            </div>
                            <button type="submit" class="btn btn-primary mb-2" name="validerCreationListe" value="$valeur">Valider</button> 
                        </form> 
                    </div>
                </div>
            </div>
        </div>
END;

        //pour supprimer une liste
        if($valeur == "modificationListe"){
            $rajout = <<<END
            <form id="f7" method="post" action="">  
                <button type="submit" class="btn btn-primary mb-2" name="supprimerListe" value="supprimer">Supprimer la liste</button> 
            </form>
            <form id="f10" method="post" action="">
                <button type="submit" class="btn btn-primary mb-2" name="rendrePublic" value="public">Rendre la liste publique</button> 
            </form>
END;
            $code = $code . $rajout;
        }
        return $code;
    }

    //pour créer ou modifier un nouvel item
    private function formulaireItem(){
        if($this->objets==null){
            $item = new Item();
            $val="creationItem";
        }else{
            $item=$this->objets["item"];
            $val="modificationItem";
        }

        $nom = $item["nom"];
        $descr = $item["descr"];
        $tarif=$item["tarif"];
        $url= $item["url"];
        $image= $item["img"];
        $res=<<<END
    <form id="f2" method="post" action="">
    
    <label class="col-md-2 mb-4" for="nomItem"> Nom:</label>
    <input id="nomItem" type="text" name="nameItem" placeholder="<nom>" class="col-md-8" value="$nom">
    
    <label class="col-md-2 mb-4" for="idDescr"> Description:</label>
    <input id="idDescr" type="text" name="descrItem" placeholder="<descr>" class="col-md-8" value="$descr">
    
    <label class="col-md-2 mb-4" for="tarif"> Tarif:</label>
    <input class="col-md-8" id = "tarif" type="number" name="tarif" placeholder="<tarif>" value="$tarif">

    <label class="col-md-2 mb-4 " for="image"> Nom ou lien de l'image</label>
    <input class="col-md-8" id="image" type = "text/url" name ="image" placeholder="<image>" value="$image">
    
    <div class="form-check mb-2">
        <input class="form-check-input" type="radio" name="exampleRadios" id="nom" value="nom" checked>
        <label class="form-check-label" for="createur">
            Nom dans la base
        </label>
    </div>
    <div class="form-check mb-2">
        <input class="form-check-input" type="radio" name="exampleRadios" id="url" value="url">
        <label class="form-check-label" for="participant">
            URL
        </label>
    </div>
    
    <input type="hidden" name="valider_form" value = "$val">
    <button type="submit" class="btn btn-primary mb-2">Valider</button>


    
    
    </form>
END;

        //pour le CSS
        $res = <<<END
        <div class="py-5 bg-light">
                    <div class="container ">
                        <div class="row bg-white border rounded">
                                <div class="col-lg-12">
                                    <h1 class="text-center pb-3">Item</h1>
                                    $res
                                </div>
                                
                         </div>
                    </div>
            </div>
END;

        return $res;
    }

    //pour afficher un seul item
    private function unItem() {
        $res= "<section>";

        //on récupère l'item
        $item = $this->objets["item"];
        $cagnotte = $this->objets["cagnotte"];

        if($item['liste_id']==0){
            $res=$res."l'item souhaite n'appartient a aucune liste et ne peut pas etre affiche";
        }else {
            $id = $item['id'];
            $nom = $item['nom'];
            $descri = $item['descr'] . ", au prix de " . $item['tarif'] . " euros";

            $reservation = $item['val'];

            if($item["urlImage"] == 1){
                $src = $item["img"];
            }else{
                $src = "img/" . $item['img'];
            }

            //$res = $res . " item id: " . $id . " de nom: " . $nom . ", voici  une courte description: " . $descri . "<img src = $src width='100' height='100'>" . "prix: " . $tarif;
        }

        //pour le formulaire
        //on récupère l'application et l'url
        $app = \Slim\Slim::getInstance();
        //on récupère le token
        $token = $this->objets["token"];
        //on récupère l'id de l'objet
        $id = $item['id'];
        //la cible du boutons
        $url = $app->urlFor("suppressionItem", ['tokenModif'=>$token, 'idItem' =>$id]);
        $urlmodif= $app->urlFor("modifItem",['tokenModif'=>$token, 'idItem' =>$id]);
        $urlAjoutImg= $app->urlFor("ajouterImage",['idItem' =>$id]);
        $urlModifImg= $app->urlFor("modifierImage",['idItem' =>$id]);
        $urlCagnotte = $app->urlFor("creerCagnotte",['token'=>$token, 'idItem'=>$id]);
        //$urlSuppCagnotte = $app->urlFor("suppCagnotte",['token'=>$token, 'idItem'=>$id]);

        //on déclare pour éviter les soucis
        $form2="";
        $form3="";
        //on vérifie si il est réservé
        if($item["reservation"] == false){

            if(isset($cagnotte)){
                $nomCagnotte=$cagnotte['nom'];
                $fcagnotte=<<<END
            <label> Cagnotte : $nomCagnotte </label>
END;

            }else{
                $fcagnotte="";
            }
            //on préremplit le nom
            $pseudo = "";
            if(isset($_SESSION["nomParticipant"])){
                $pseudo = $_SESSION["nomParticipant"];
            }

            $form = <<<END
                <form id="f2" method="post" action = "$url" >
                    <div class="row">
                         <button type="submit" name ="" class="btn btn-primary mb-2">Supprimer cet item</button>
                    </div>
                </form>
                
               
                
END;

            $form2 =<<<END
 <form id="f2" method="get" action = "$urlmodif" >
                    <div class="row">
                    <input type="hidden" name="valider_form" value = "Modifier Item">
                         <button type="submit"  class="btn btn-primary mb-2">Modifier cet item</button>
                    </div>
                </form>
END;

            $form4 =<<<END
 <form id="f2" method="get" action = "$urlCagnotte" >
                    <div class="row">
                    <input type="hidden" name="cagnotte" value = "Créer Cagnotte">
                         <button type="submit"  class="btn btn-primary mb-2">Créer une cagnotte</button>
                    </div>
                </form>


END;





            if(!isset($item['img'])){
                $form3 =<<<END
 <form id="f2" method="get" action = "$urlAjoutImg" >
                    <div class="row">
                    <input type="hidden" name="image" value = "Image Item">
                         <button type="submit"  class="btn btn-primary mb-2"> Ajouter Image</button>
                    </div>
                </form>
            
           
END;
                }else{
                $form3 =<<<END
 <form id="f2" method="get" action = "$urlModifImg" >
                    <div class="row">
                    <input type="hidden" name="modifImage" value = "Image Item">
                         <button type="submit"  class="btn btn-primary mb-2"> Modifier Image </button>
                    </div>
                </form>
            
           
END;

            }

        }else{
            //on vérifie si on est arrivé à échance
            if(!isset($_COOKIE[$item["liste_id"]])){
                //on récupère la réservation
                $res = Reservation::where('item_id', '=', $item["id"])->first();
                $nomReser = $res["nomParticipant"];
                $message = $this->objets["message"]["message"];

                //le code
                $form = <<<END
            <h5 class="text-center"> Objet réservé par $nomReser </h5>
            <h6>$message</h6>
END;
            }else{
                $form = "";
            }

        }


        $res = <<<END
        <div class="py-5 bg-light">
                <div class="container ">
                    <div class="row bg-white border rounded">
                            <div class="col-lg-5">
                                <img src="$src" class="img-fluid presentationItem">
                            </div>
                            <div class="col-lg-7">
                                <h1 class="text-center">$nom</h1>
                                <p>$descri</p>
                                $form
                                $form2
                                $form3
                                $form4
                                $fcagnotte
                                
                            </div>
                            
                     </div>
                </div>
        </div>
END;

        return $res;
    }

    private function formulaireImage()
    {
        if ($this->objets == null) {
            $img = "";
            $val = "creationImage";

        } else {
            $img = $this->objets['image'];
            $val = "modificationImage";

        }
        $res = <<<END
        
    <form id="fimg" method="post" action="" enctype="multipart/form-data">
        <label class="col-md-2 mb-4" for="image"> Image dans la base:</label>
        <input class="col-md-8" id="image" type = "text/url" name ="image" placeholder="<image>">
        
        <h5 class="col-md-5 mb-4">Ou télécharger une image</h5>
        <input class="col-md-10 mb-3" type="file" name="fileToUpload" id="fileToUpload">
        
        <button class="btn btn-primary" type="submit" name="valider_form" value="$val">Valider</button>
    </form>

    


END;

        //pour le css
        $res = <<<END
        <div class="py-5 bg-light">
                    <div class="container ">
                        <div class="row bg-white border rounded">
                                <div class="col-lg-12">
                                    <h1 class="text-center pb-3">Modifier Image</h1>
                                    $res
                                </div>
                                
                         </div>
                    </div>
            </div>
END;

        return $res;
    }

    private function itemSansListe(){
        //on récupère l'item
        $item = $this->objets["item"];

        if($item['liste_id']==0){
        }else {
            $nom = $item['nom'];
            $descri = $item['descr'] . ", au prix de " . $item['tarif'] . " euros";


            if($item["urlImage"] == 1){
                $src = $item["img"];
            }else{
                $src = "img/" . $item['img'];
            }

        }

        //pour les url
        $app = Slim::getInstance();
        $id = $item['id'];

        $urlAjoutImg= $app->urlFor("ajouterImage",['idItem' =>$id]);
        $urlModifImg= $app->urlFor("modifierImage",['idItem' =>$id]);


        if(!isset($item['img'])){
            $form =<<<END
 <form id="f2" method="get" action = "$urlAjoutImg" >
                    <div class="row">
                    <input type="hidden" name="valider_form" value = "Image Item">
                         <button type="submit"  class="btn btn-primary mb-2"> Ajouter Image</button>
                    </div>
                </form>
            
           
END;
        }else{
            $form =<<<END
 <form id="f2" method="get" action = "$urlModifImg" >
                    <div class="row">
                    <input type="hidden" name="valider_form" value = "Image Item">
                         <button type="submit"  class="btn btn-primary mb-2"> Modifier Image </button>
                    </div>
                </form>
            
           
END;

        }





        $res = <<<END
        <div class="py-5 bg-light">
                <div class="container ">
                    <div class="row bg-white border rounded">
                            <div class="col-lg-5">
                                <img src="$src" class="img-fluid presentationItem">
                            </div>
                            <div class="col-lg-7">
                                <h1 class="text-center">$nom</h1>
                                <p>$descri</p>
                                $form
                            </div>
                            
                     </div>
                </div>
        </div>
END;

        return $res;
    }


    //pour joindre une liste déjà existante
    private function formulaireJoindreUneListe(){
        $html = <<<END
            <form id="f11" method="post" action="" class="col-md-10">
                <div class="col-md-4 mb-2">
                    <label for = "token">Token </label>
                    <input type="text" name="token" class="form-control form-control-sm" id="token" placeholder="token de modification" value="">
                </div>
                <div class="col-md-4 mb-2">
                    <label for = "numListe">Numéro de la liste </label>
                    <input type="number" name="numListe" placeholder="Numéro de la liste" class="form-control" value=""> 
                </div>
                <div class="col-md-2 mb-2">
                    <button type="submit" name="valider_form" value="">Valider</button>
                </div>
            </form>
END;

            //on renvoie le html
        $html = <<<END
        <div class="py-5 bg-light">
                <div class="container ">
                    <div class="row bg-white border rounded">
                            <div class="col-lg-12">
                                <h1 class="text-center"> Joindre une liste</h1>
                                $html
                            </div>
                            
                     </div>
                </div>
        </div>
END;


        return $html;
    }

    private function urlPartage(){
        $app = \Slim\Slim::getInstance();
        $id=$this->objets['id'];
        $token=$this->objets['token'];
        $url = $app->urlFor("afficherListe", ['token' => $token, 'idListe' =>$id]);
        $res = <<<END
            <div class="py-5 bg-light">
                    <div class="container ">
                        <div class="row bg-white border rounded">
                                <div class="col-lg-12">
                                    <h1 class="text-center pb-3">Lien de partage:</h1>
                                    <p>localhost$url</p>
                                </div>
                                
                         </div>
                    </div>
            </div>
        
END;
        return $res;
    }

    private function afficherItem(){
        $items = $this->objets["items"];

        $ch = "";
        foreach ($items as $item){
            $infos = $item["nom"] . ", " . $item["descr"];
            $id = $item["id"];
            $ch = $ch . <<<END
            <form method="post">
                <label> $infos
                    
                </label>
                <button type="submit" class="btn btn-primary" name ="ajout" value="$id">Ajouter</button>
            </form>
END;

        }

        if(empty($items)){
            $ch = $ch . "Vous n'avez pas créer d'item";
        }

        return $ch;
    }

    public function formulaireCagnotte(){
        $app = \Slim\Slim::getInstance();
        $token=$this->objets['token'];
        $item=$this->objets['item'];
        $liste = Liste::where('no','=',$item['liste_id'])->first();
        $c = $this->objets['cagnotte'];
   $urlCagnotte = $app->urlFor("creerCagnotte",['token'=>$token, 'idItem'=>$item['id']]);


   if($c==false){
       $res = <<<END
            <form id="fcagnotte" method="post" action="$urlCagnotte">
    
    <label> nom de la cagnotte:
    <input type="text" name="nomCagnotte" placeholder="<nom>" >
    </label>
    
    <button type="submit" name="" value="valid_formCagnotte">Valider</button>
    
    </form>
    
    
END;

   }else{
       $res = <<<END
            <label> erreur : la cagnotte existe déjà</label>
    
END;
   }

        return $res;

    }

    public function render($select){
        switch ($select){
            case 1:
                $content = $this->formulaireJoindreUneListe();
                break;
            case 2:
                $content = $this->itemSansListe();
                break;
            case 3:
                $content = $this->afficherItem();
                break;
            case 5:
                $content = $this->afficherFormulaireListe();
                break;
            case 6:
                $content = $this->formulaireItem();
                break;
            case 7:
                $content = $this->listeDetaille();
                break;
            case 10:
                $content =$this->unItem();
                break;
            case 11:
                $content = $this->formulaireImage();
                break;
            case 12:
            	$content = $this->urlPartage();
            	break;
            case 13:
                $content = $this->formulaireCagnotte();
                break;
        }

        //l'url pour les headers
        $app = \Slim\Slim::getInstance();
        $urlCre = $app->urlFor("creationListe");
        $urlParticipation = $app->urlFor("participations");
        $urlConnexion = $app->urlFor("connexion");
        $urlAccueil = $app->urlFor("accueil");
        $urlChercher = $app->urlFor("rechercher");
        $urlCreerItem = $app->urlFor("creerItem");
        $urlCreateurs = $app->urlFor("createurs");
        $urlJoin = $app->urlFor("joindreUneListe");

        //si il est connecté
        if(isset($_SESSION["login"])){
            $nom = $_SESSION["login"];

            //on affiche son nom
            $accroche = <<<END
            <li class="nav-item">
                <a class="nav-link" href="$urlConnexion">Bienvenue $nom </a>
            </li>
END;

            if($_SESSION["type"] == "participant"){
                $particularite = <<<END
                <li class="nav-item">
                    <a class="nav-link" href="$urlParticipation"> Mes participations</a>
                </li>
END;
            }else {
                $particularite = <<<END
                <li class="nav-item">
                    <a class="nav-link" href="$urlCre"> Créer une liste</a>
                </li>
END;
            }
        }else{
            $accroche = "";
            $particularite = <<<END
                <li class="nav-item">
                    <a class="nav-link" href="$urlCre"> Créer une liste</a>
                </li>
END;
        }

        //on remplit le html
        $html = <<<END
        <!DOCTYPE html>
        <html>
            <head>
                <meta charset="UTF-8">
                <meta http-equiv="X-UA-Compatible" content="IE=edge">
                <meta name="viewport" content="width=device-width, initial-scale=1">
        
                <link href="bootstrap/css/bootstrap.css" rel="stylesheet">
                <link href="bootstrap/css/perso.css" rel="stylesheet">
            </head>
        <body>
        
        <header>
            <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
                <a class="navbar-brand" href="$urlAccueil">Wishlist</a>
                <div class="collapse navbar-collapse">
                    <ul class="navbar-nav col-md-12">
                        $particularite
                        <li class="nav-item">
                            <a class="nav-link" href="$urlConnexion"> Se connecter</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="$urlCreerItem"> Créer un item</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="$urlCreateurs"> Créateurs</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="$urlJoin"> Joindre une liste</a>
                        </li>
                        $accroche
                        <form class="form-inline my-1 my-lg-0" action="$urlChercher">
                            <button class="btn btn-outline-info my-1 my-sm-0 " type="submit">Rechercher</button>
                        </form>
                    </ul>
                </div>
            </nav>
        </header>

            $content
        </body>
        </html>

END;

        echo $html;
    }
}
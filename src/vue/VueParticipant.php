<?php
/**
 * Created by PhpStorm.
 * User: gabrielsega
 * Date: 04/12/2018
 * Time: 17:21
 */

namespace wishlist\vue;

use Slim\Slim;
use wishlist\modele\Cagnotte;
use wishlist\modele\Liste;
use wishlist\modele\Reservation;

class VueParticipant{
    private $objets;

    public function __construct($obj) {
        $this->objets = $obj;
    }

    //pour un probleme d'accès
    private function accesRefuse(){
        $html = "Erreur 403 accès refusé : vous n'avez pas accès à cette page";
        return $html;
    }

    //pour lister les objets d'une liste
    private function listeDetaille(){
        //pour accéder à l'application
        $app = \Slim\Slim::getInstance();

        //on ajoute les infos de la liste
        $l = $this->objets['liste'];
        $titre = $l["titre"];
        $desc = $l['description'];
        $exp = $l['expiration'];
        $phrase = $desc . ", vous avez jusqu'au $exp pour participer à cette liste";

        $deb = <<<END
        <section class="jumbotron text-center">
            <div class="container">
                <h1>$titre</h1>
                <p class="lead text-muted"> $phrase</p>
            </div>
        </section>
END;
		$aff='<br>';
        /*foreach ($this->objets['message'] as $message){
        	$mes= $message["message"];
        	
        	$affiche= <<<END
			$mes
END;

        	$aff = $aff."<br>".$affiche. "<br>";
        	
        }
        $deb=$deb.$aff;*/
        

        $ch = $deb . "\n" . "<div class='album py-5 bg-light'> \n <div class='container'> \n <div class='row'>";

        // on ajoute les items de la liste
        foreach ($this->objets['item'] as $obj){
            //url de redirection
            $url = $app->urlFor("item", ['id'=>$obj['id'], 'token' => $this->objets['token']]);

            //pour l'url de son image
            if($obj["urlImage"] == 1){
                $src = $obj["img"];
            }else{
                $src = "img/" . $obj['img'];
            }

            //pour ses infos
            $nom = $obj["nom"];
            $desc = $obj["descr"];
            $tarif = $obj['tarif'];

            //pour son état de réservation
            if($obj['reservation'] == 0){
                $res ="<br><br> Pas réservé";
            }else{
                $res =" <br><br> Réservé";
            }

            $text = $desc . ", " . $tarif . $res;

            $ob = <<<END
            <div class="col-md-3 card-group">
                <div class="card ">
                    <img class="card-img-top img-fluid" src="$src">
                    <div class="card-body " >
                        <h5 class="card-title">$nom</h5> 
                        <p class="card-text "> $text </p>
                    </div>
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item text-center">                                            
                            <a class="btn btn-primary" href="$url">Voir les détails</a>
                        </li>
                    </ul>
                </div>
            </div>
END;
            $ch = $ch . "\n" . $ob . "\n";

        }

        //Redirection pour message
        $urlMess = $app->urlFor("ajouterMessage", ["numListe"=>$l["no"]]);

        //on ajoute une carte pour certaines actions
        $ch = $ch . <<<END
            <div class="col-md-3 ">
                <div class="card ">
                    <div class="card-body " >
                        <h5 class="card-title">Options</h5> 
                    </div>
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item text-center">  
                            <form name="ajouterMessage" method="post">
                                <a class="btn btn-primary" href="$urlMess">Ajouter un message</a>
                            </form>                                          
                        </li>
                    </ul>
                </div>
            </div>
END;

        //on ajoute tous les messages
        foreach ($this->objets['message'] as $message){
            $mes= $message["message"];

            $ch = $ch . <<<END
            <div class="col-md-3 ">
                <div class="card ">
                    <div class="card-body " >
                        <h5 class="card-title">Message</h5> 
                        <p class="card-text "> $mes </p>
                    </div>
                </div>
            </div>
END;


        }

        $aff='<br>';
        /*foreach ($this->objets['message'] as $message){
        	$mes= $message["message"];

        	$affiche= <<<END
			$mes
END;

        	$aff = $aff."<br>".$affiche. "<br>";

        }
        $deb=$deb.$aff;*/


        $ch = $ch . "</div>" . "\n" . "</div>" . "\n" . "</div>";
        return $ch;
    }


    private function listeListes (){
        $res="<section>";
        foreach ($this->objets as $liste) {
            $res=$res."Liste num: ".$liste['no'].", dont le nom est: ".$liste['titre']."<br>";
        }
        return $res;
    }

    private function listeItems(){
        $res="<section>";
        foreach ($this->objets as $item) {
            $res=$res."Item num: ".$item['id'].", dont le nom est: ".$item['nom']."<br>";
        }
        return $res;
    }

    private function unItem() {
        $res= "<section>";

        //on récupère l'item
        $item = $this->objets["item"];


        if($item['liste_id']==0){
            $res=$res."l'item souhaite n'appartient a aucune liste et ne peut pas etre affiche";
        }else {
            $id = $item['id'];
            $nom = $item['nom'];
            $descri = $item['descr'] . ", au prix de " . $item['tarif'] . " euros";

            $reservation = $item['val'];

            if($item["urlImage"] == 1){
                $src = $item["img"];
            }else{
                $src = "img/" . $item['img'];
            }
            //$res = $res . " item id: " . $id . " de nom: " . $nom . ", voici  une courte description: " . $descri . "<img src = $src width='100' height='100'>" . "prix: " . $tarif;
        }

        //pour le formulaire
        //on récupère l'application et l'url
        $app = \Slim\Slim::getInstance();
        $token = $this->objets["token"];
        $id = $this->objets["item"]["id"];
        $url = $app->urlFor("traiterReservation", ["tokenAcces"=>$token, "idItem"=>$id]);

        //on vérifie si il est réservé
        if($item["reservation"] == false){
            //on préremplit le nom
            $pseudo = "";
            if(isset($_SESSION["nomParticipant"])){
                $pseudo = $_SESSION["nomParticipant"];
            }elseif (isset($_SESSION["login"])){
                $pseudo = $_SESSION["login"];
            }

            $form = <<<END
            <h5 class="text-center"> Réserver cet item</h5>
                <form id="f4" method="post" action = "$url" >
                    <div class="row">
                        <label for="pseudo" class="col-sm-4 col-form-label">Nom du participant</label>
                        <div class="col-lg-6">
                            <input type="text" name="nomParticipant" class="form-control form-control-sm" id="pseudo" placeholder="Nom" value="$pseudo">
                        </div>
                         <label for="message" class="col-sm-4 col-form-label">Message</label>
                        <div class="col-lg-6">
                            <input type="text" name="msg" class="form-control form-control-sm" id="message" placeholder="Votre message">
                        </div>
                        <div class="col-md-2">
                            <button type="submit" name ="validerReservation" class="btn btn-primary mb-2">Réserver</button>
                        </div>
                     </div>
                </form>
END;

        }else{
            //on récupère la réservation
            $res = Reservation::where('item_id', '=', $item["id"])->first();
            $nomReser = $res["nomParticipant"];
            $message = $this->objets["message"]["message"];

            //le code
            $form = <<<END
            <h5 class="text-center"> Objet réservé par $nomReser </h5>
            <h6>$message</h6>
END;

        }


        $res = <<<END
        <div class="py-5 bg-light">
                <div class="container ">
                    <div class="row bg-white border rounded">
                            <div class="col-lg-5">
                                <img src="$src" class="img-fluid presentationItem">
                            </div>
                            <div class="col-lg-7">
                                <h1 class="text-center">$nom</h1>
                                <p>$descri</p>
                                $form
                            </div>
                            
                     </div>
                </div>
        </div>
END;

        return $res;
    }

    private function formulaireMessage(){
        $form=<<<END
    <form id="f3" class="py-2" method="post" action="">
        <label class="col-md-12"> Message:
            <input class="col-md-10" type="text" name="Message" placeholder="Ecrivez votre message ici">
        </label>
        </br>	
        </br>			
        <button type="submit" name="valider_form" class="btn btn-primary" value="valid_formulaireMessage">Valider</button>
    </form>
END;

        $res = <<<END
        <div class="py-5 bg-light">
                <div class="container ">
                    <div class="row bg-white border rounded">
                            <div class="col-lg-12">
                                <h1 class="text-center">Votre message</h1>
                                $form
                            </div>
                            
                     </div>
                </div>
        </div>

END;



        return $res;
    }

    private function afficherMessagesListe(){
        $res="<section>";
        foreach ($this->objets as $mes) {
            var_dump($mes);
            //$res=$res.$mes["message"]."<br><br>";
        }
        $res= $res . "</section>";
        return $res;
    }

    private function afficherParticipations(){
        $html = "<h1 class='text-center'>Voici vos participations jusqu'à présent</h1>";
        $app = Slim::getInstance();

        if($this->objets != null){
            foreach ($this->objets as $asso){
                //on récupère la liste puis l'item
                $item = $asso["item"];
                $liste = $asso["liste"];
                $nom = $item["nom"];
                $num = $liste["no"];
                $token = $liste["tokenAcces"];

                $url = $app->urlFor("item", ["token"=>$liste["tokenAcces"], "id"=>$item["id"]]);
                $html = $html. "<a href='$url'> $nom de la liste numéro $num</a> ";

            }
        }

        $html = <<<END
        <div class="py-5 bg-light">
                <div class="container ">
                    <div class="row bg-white border rounded">
                            <div class="offset-3">
                                $html
                            </div>
                            
                     </div>
                </div>
        </div>
END;


        return $html;
    }

    private function probleme(){
        $html = $this->objets["probleme"];
        return $html;
    }

    public function participerCagnotte(){
        $app = \Slim\Slim::getInstance();
        $item= $this->objets['item'];
        $nomItem= $item['nom'];
        $tarif=$item['tarif'];

        $token = $this->objets['token'];
        $c = Cagnotte::where('item_id','=',$item['id'])->first();
        $montantCagnotte = $c['montant'];
        $nomCagnotte = $c['nom'];
        $src = "img/" . $item['img'];
        $url = $app->urlFor('participerCagnotte',['token'=>$token, 'idItem'=>$item['id']]);

        $form=<<<END
                <form id="f4" method="post" action = "$url" >
                    <div class="row">
                        <label for="nomCagnotte" class="col-sm-4 col-form-label">Nom de la cagnotte</label>
                        <div class="col-lg-6">
                            <label>$nomCagnotte</label>
                        </div>
                         <label for="montant" class="col-sm-4 col-form-label">Montant</label>
                        <div class="col-lg-6">
                            <input type="number" name="montant" class="form-control form-control-sm" placeholder="Votre montant" min= "0" max="$tarif">
                        </div>
                        <div class="col-md-2">
                            <button type="submit" name ="validerParticipationCagnotte" class="btn btn-primary mb-2">Participer</button>
                        </div>
                     </div>
                </form>
END;

        $res=<<<END
         <div class="py-5 bg-light">
                <div class="container ">
                    <div class="row bg-white border rounded">
                            <div class="col-lg-5">
                                <img src="$src" class="img-fluid presentationItem">
                                
                            </div>
                            <div class="col-lg-7">
                                <h1 class="text-center">$nomItem</h1>
                                 <h5 class="text-center"> Participer à la cagnotte</h5>
                                 <label> Montant sur la cagnotte: $montantCagnotte euros </label> <br>
                                 <label> Tarif : $tarif euros</label>
                                $form
                     </div>
                </div>
        </div>
           
END;





return $res;
    }



    public function render($select){
        switch ($select){
            //pour les accès refusés
            case -2:
                $content = $this->probleme();
                break;
            case -1:
                $content = $this->accesRefuse();
                break;
            case 1 :
                $content = $this->listeListes();
                break;

            case 2:
                $content = $this->listeDetaille();
                break;

            case 3:
                $content =$this->unItem();
                break;

            case 4 :
                $content = $this->listeItems();
                break;

            case 5 :
                $content = $this->afficherParticipations();
                break;


            case 8:
                $content = $this->formulaireMessage();
                break;

            case 9:
                $content = $this->afficherMessagesListe();
                break;

                case 10:
                    $content =$this->participerCagnotte();
                    break;


        }

        //pour le menu
        $app = \Slim\Slim::getInstance();
        $urlCre = $app->urlFor("creationListe");
        $urlParticipation = $app->urlFor("participations");
        $urlConnexion = $app->urlFor("connexion");
        $urlAccueil = $app->urlFor("accueil");
        $urlChercher = $app->urlFor("rechercher");
        $urlCreerItem = $app->urlFor("creerItem");
        $urlCreateurs = $app->urlFor("createurs");
        $urlJoin = $app->urlFor("joindreUneListe");

        //si il est connecté
        if(isset($_SESSION["login"])){
            $nom = $_SESSION["login"];

            //on affiche son nom
            $accroche = <<<END
            <li class="nav-item">
                <a class="nav-link" href="$urlConnexion">Bienvenue $nom </a>
            </li>
END;
            if($_SESSION["type"] == "participant"){
                $particularite = <<<END
                <li class="nav-item">
                    <a class="nav-link" href="$urlParticipation"> Mes participations</a>
                </li>
END;
            }else{
                $particularite = <<<END
                <li class="nav-item">
                    <a class="nav-link" href="$urlCre"> Créer une liste</a>
                </li>
END;

            }
        }else{
            $accroche = "";
            $particularite = <<<END
                <li class="nav-item">
                    <a class="nav-link" href="$urlCre"> Créer une liste</a>
                </li>
END;
        }

        //on remplit le html
        $html = <<<END
        <!DOCTYPE html>
        <html>
            <head>
                <meta charset="UTF-8">
                <meta http-equiv="X-UA-Compatible" content="IE=edge">
                <meta name="viewport" content="width=device-width, initial-scale=1">
        
                <link href="bootstrap/css/bootstrap.css" rel="stylesheet">
                <link href="bootstrap/css/perso.css" rel="stylesheet">
            </head>
        <body>
        
        <header>
            <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
                <a class="navbar-brand" href="$urlAccueil">Wishlist</a>
                <div class="collapse navbar-collapse">
                    <ul class="navbar-nav col-md-12">
                        $particularite
                        <li class="nav-item">
                            <a class="nav-link" href="$urlConnexion"> Se connecter</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="$urlCreerItem"> Créer un item</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="$urlCreateurs"> Créateurs</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="$urlJoin"> Joindre une liste</a>
                        </li>
                        
                        $accroche
                        <form class="form-inline my-1 my-lg-0" action="$urlChercher">
                            <button class="btn btn-outline-info my-1 my-sm-0 " type="submit">Rechercher</button>
                        </form>
                    </ul>
                </div>
            </nav>
        </header>

            $content
        </body>
        </html>

END;

        echo $html;
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: gabrielsega
 * Date: 08/01/2019
 * Time: 11:04
 */


namespace wishlist\vue;
use Slim\Slim;
use wishlist\modele\Liste;
use wishlist\modele\Item;

class VueGeneral {
    private $objets;

    public function __construct($obj) {
        $this->objets = $obj;
    }

    private function afficherAccueil(){
        $res = <<<END
        <h1> Bienvenue sur whishlist</h1>
END;

        return $res;
    }

    private function afficherToutPublic(){
        $app = Slim::getInstance();

        if(isset($this->objets["nom"])){
            $nom = $this->objets["nom"];
            $listes = $this->objets["listes"];
        }else{
            $nom = "";
            $listes = $this->objets;
        }

        //le code de base
        $html = "<h1> Voici l'ensemble des listes publiques </h1>";

        //pour afficher le formulaire de recherche
        $html = $html . <<<END
        <form id="" method="post" action="">
            <input type="text" placeholder="nom de l'auteur" name="nom" value = "$nom"> 
            <button type="submit" class="btn btn-primary mb-2" name="recherche" value="">Rechercher</button> 
         </form>
END;


        //on parcourt toutes les listes
        foreach ($listes as $liste){
            //on récupère l'url
            $url = $app->urlFor("afficherListe", ["token"=>$liste->tokenAcces, "idListe"=>$liste->no]);
            $titre = $liste->titre;
            $html = $html . <<<END
            <a href="$url">$titre</a>
            <br>
END;

        }

        return $html;
    }

    private function afficherMessage(){
        return $this->objets;
    }

    private function afficherCreateurs(){
        $html = <<<END
        <h1 class="text-center col-md-12"> Voici les créateurs ayant au moins une liste publique </h1>
END;
        foreach ($this->objets as $cre){
            $login = $cre["login"];
            $html = $html . <<<END
            <h2>$login</h2>
            <br>
END;

        }

        //css
        $html = <<<END
        <div class="py-5 bg-light">
                <div class="container">
                    <div class="row bg-white border rounded">
                            <div class="">
                                $html
                            </div>
                            
                     </div>
                </div>
        </div>
END;


        return $html;
    }


    public function render($select){
        switch ($select){
            case 1:
                $content = $this->afficherAccueil();
                break;
            case 2:
                $content = $this->afficherToutPublic();
                break;
            case 3:
                $content = $this->afficherMessage();
                break;
            case 4:
                $content = $this->afficherCreateurs();
                break;
        }

        //l'url pour les headers
        $app = \Slim\Slim::getInstance();
        $urlCre = $app->urlFor("creationListe");
        $urlParticipation = $app->urlFor("participations");
        $urlConnexion = $app->urlFor("connexion");
        $urlAccueil = $app->urlFor("accueil");
        $urlChercher = $app->urlFor("rechercher");
        $urlCreerItem = $app->urlFor("creerItem");
        $urlCreateurs = $app->urlFor("createurs");
        $urlJoin = $app->urlFor("joindreUneListe");

        //si il est connecté
        if(isset($_SESSION["login"])){
            $nom = $_SESSION["login"];

            //on affiche son nom
            $accroche = <<<END
            <li class="nav-item">
                <a class="nav-link" href="$urlConnexion">Bienvenue $nom </a>
            </li>
END;

            if($_SESSION["type"] == "participant"){
                $particularite = <<<END
                <li class="nav-item">
                    <a class="nav-link" href="$urlParticipation"> Mes participations</a>
                </li>
END;
            }else {
                $particularite = <<<END
                <li class="nav-item">
                    <a class="nav-link" href="$urlCre"> Créer une liste</a>
                </li>
END;
            }
        }else{
            $accroche = "";
            $particularite = <<<END
                <li class="nav-item">
                    <a class="nav-link" href="$urlCre"> Créer une liste</a>
                </li>
END;
        }

        //on remplit le html
        $html = <<<END
        <!DOCTYPE html>
        <html>
            <head>
                <meta charset="UTF-8">
                <meta http-equiv="X-UA-Compatible" content="IE=edge">
                <meta name="viewport" content="width=device-width, initial-scale=1">
        
                <link href="bootstrap/css/bootstrap.css" rel="stylesheet">
                <link href="bootstrap/css/perso.css" rel="stylesheet">
            </head>
        <body>
        
        <header>
            <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
                <a class="navbar-brand" href="$urlAccueil">Wishlist</a>
                <div class="collapse navbar-collapse">
                    <ul class="navbar-nav col-md-12">
                        $particularite
                        <li class="nav-item">
                            <a class="nav-link" href="$urlConnexion"> Se connecter</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="$urlCreerItem"> Créer un item</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="$urlCreateurs"> Créateurs</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="$urlJoin"> Joindre une liste</a>
                        </li>
                        $accroche
                        <form class="form-inline my-1 my-lg-0" action="$urlChercher">
                            <button class="btn btn-outline-info my-1 my-sm-0 " type="submit">Rechercher</button>
                        </form>
                    </ul>
                </div>
            </nav>
        </header>

            $content
            
        </body>
        </html>

END;

        echo $html;
    }
}
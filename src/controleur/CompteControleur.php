<?php
/**
 * Created by PhpStorm.
 * User: gabrielsega
 * Date: 08/01/2019
 * Time: 11:11
 */

namespace wishlist\controleur;


use Slim\Slim;
use wishlist\modele\Compte;
use wishlist\modele\Liste;
use wishlist\modele\Message;
use wishlist\vue\VueCompte;

class CompteControleur{
    public function formulaireCreation(){
        //on instancie la vue
        $vue = new VueCompte(null);

        //on affiche le formulaire
        $vue->render(1);
    }

    public function formulaireConnexion(){
        //on instancie la vue
        $vue = new VueCompte(null);

        //on affiche le formulaire
        $vue->render(2);
    }

    public function formulaireDeconnexion(){
        //on instancie la vue
        $vue = new VueCompte(null);

        //on affiche le formulaire
        $vue->render(3);
    }

    public function traiterFormulaireCreation(){
        //on récupère l'application
        $app = \Slim\Slim::getInstance();

        //on vérifie que lien vient bien de la création
        if(isset($_POST["creationCompte"])){
            $pseudo = $_POST["pseudo"];
            $mdp = $_POST["mdp"];

            //on hash le mdp
            $mdp = password_hash($mdp, PASSWORD_DEFAULT);

            //on sauvegarde dans la base
            $compte = new Compte();
            $compte->mdp = $mdp;
            $compte->login = $pseudo;

            //on regarde le type de compte
            if(isset($_POST["exampleRadios"])){
                $type = $_POST["exampleRadios"];
            }
            $compte->type = $type;

            $compte->save();

            //on redirige vers la connexion
            $url = $app->urlFor("connexion");
            $app->redirect($url);
        }
    }

    public function afficherModif(){
        //on vérifie que quelqu'un est bien connecté
        if(isset($_SESSION["login"])){
            //on créé la vue et affiche le foprmulaire
            $vue = new VueCompte(null);
            $vue->render(4);
        }
    }

    public function traiterModif(){
        //pour la redirection
        $app = Slim::getInstance();

        //on vérifie qu'on provient bien de la modification de compte
        if(isset($_POST["modifierCompte"]) || isset($_POST["supprimerCompte"])){
            //on récupère le compte
            $compte = Compte::find($_SESSION["idCompte"]);

            //on vérifie le mot de passe
            if(password_verify($_POST["ex-mdp"], $compte->mdp)){

                //si c'est une modification
                if(isset($_POST["modifierCompte"])){
                    //on change le mdp de la base
                    $compte->mdp = password_hash($_POST["mot"], PASSWORD_DEFAULT);
                    $compte->save();

                    //on détruit la session
                    session_destroy();
                }else{
                    //on retourne vers l'accueil
                    $url = $app->urlFor("accueil");

                    //on récupère toutes ses listes
                    $listes = Liste::where("user_id", "=", $compte->idCompte)->get();
                    foreach ($listes as $liste){
                        //on vérifie si on était à échéance ou pas
                        $eche = strtotime($liste->expiration);
                        if(time()<=$eche){
                            //on récupère ses items
                            $items = $liste->item;

                            //on les parcourt
                            foreach ($items as $item){
                                //on supprime le message de l'item
                                $message = Message::where("item_id", "=", $item->id);
                                $message->delete();

                                //on supprime
                                $item->delete();
                            }

                            //on récupère ses messages
                            $messages = $liste->message;

                            //on les parcourt
                            foreach ($messages as $message){
                                $message->delete();
                            }

                            //on les parcourt
                            foreach ($items as $item){
                                //on supprime le message de l'item
                                $message = Message::where("item_id", "=", $item->id);
                                $message->delete();

                                //on supprime
                                $item->delete();
                            }



                            //on supprime la liste
                            $liste->delete();
                        }
                    }



                    //on supprime le compte
                    $compte->delete();

                    //on détruit la session
                    session_destroy();

                    //on redirige
                    $app->redirect($url);
                }



                //on redirige
                $url = $app->urlFor("connexion");
                $app->redirect($url);
            }else{
                $vue = new VueCompte(null);
                $vue->render(-1);
            }
        }else{
            $vue = new VueCompte(null);
            $vue->render(-2);
        }
    }

    public function traiterFormulaireConnexion(){
        //on récupère l'application
        $app = \Slim\Slim::getInstance();

        //on vérifie que lien vient bien de la création
        if(isset($_POST["connexion"])){
            $pseudo = $_POST["pseudo"];
            $mdp = $_POST["mdp"];

            //on récupère le pseudo correspondant dans la base
            $compte = Compte::where('login', "=", $pseudo)->first();

            //vérifie le mdp
            if(password_verify($mdp, $compte->mdp)){
                $_SESSION["idCompte"] = $compte->idCompte;
                $_SESSION["login"] = $compte->login;
                $_SESSION["mdp"] = $compte->mdp;
                $_SESSION["type"] = $compte->type;
            }
        }

        //on redirige vers l'accueil
        $url = $app->urlFor("accueil");
        $app->redirect($url);
    }

    public function traiterFormulaireDeConnexion(){
        //on récupère l'application
        $app = \Slim\Slim::getInstance();

        //on détruit la session
        session_destroy();

        //on redirige vers la page de connexion
        $url = $app->urlFor("connexion");
        $app->redirect($url);
    }
}
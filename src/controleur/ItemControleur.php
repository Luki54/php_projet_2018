<?php

namespace wishlist\controleur;

use wishlist\modele\Cagnotte;
use wishlist\modele\Compte;
use wishlist\modele\Message;
use wishlist\modele\Reservation;
use wishlist\vue\VueCreateur;
use \wishlist\vue\VueParticipant;
use \wishlist\modele\Item;
use \wishlist\modele\Liste;

class ItemControleur{

    public function getAllItems(){
        $items = Item::get();
        $vue=new VueParticipant($items->toArray());
        $vue->render(4);
    }



	
	public function getUnItem($id, $token) {
	    $item = item::where('id','=', $id)->first();
	    $idlist= $item['liste_id'];
	    $tab["item"] = $item;
	    $tab["token"] = $token;


        $tab['cagnotte']= Cagnotte::where('item_id','=',$id)->first();

        $tab["message"] = Message::where('item_id', '=', $item["id"])->first();

	    $vue = new VueCreateur($tab);

	    
	    $list=Liste::where('no','=',$idlist)->first();
	    
	    if($token == $list["tokenAcces"]){
	        //on affiche la bonne vue

            $vue = new VueParticipant($tab);
            $vue->render(3);
        }else if($token == $list["tokenModif"]){
            //on affiche la vue permettant de supprimer l'item
            $vue->render(10);
        }elseif($token == 0) {
            $vue->render(2);
        }else{
            //on affiche une page d'erreur
            $vue->render(-1);
        }
    }

    public function traiterSuppressionItem($tokenModif, $idItem){
        //on récupère l'application
        $app = \Slim\Slim::getInstance();

        //on récupère l'item
        $item = Item::find($idItem);
        //on récupère sa liste
        $liste = Liste::find($item["liste_id"]);

        //on vérifie qu'on a bien le bon token
        if($tokenModif == $liste["tokenModif"]){
            //si c'est le cas on vérifie qu'il n'est pas réservé
            if($item["reservation"] == false) {
                //on l'efface
                $item->delete();
            }
        }

        //dans tous les cas on redirige vers la liste dont faisait partit l'item
        $url = $app->urlFor("afficherListe", ["token"=>$tokenModif, "idListe" => $liste["no"]]);

        //on redirige
        $app->redirect($url);
    }

    public function creationItem() {
        $vue = new VueCreateur(null);
        $vue->render(6);
    }

    public function modifItem($tokenModif,$id){
        //on récupère l'application
        $app = \Slim\Slim::getInstance();

            $item= item::find($id);

            $liste = Liste::find($item["liste_id"]);

            if($tokenModif==$liste["tokenModif"] and $item["reservation"]==false){
                //si bon token et pas reserve on peut modifier item
                $tab["item"]=$item;
                $tab["token"]=$tokenModif;
                $vue=new VueCreateur($tab);
                $vue->render(6);
            }else{
                //sinon on renvoi a la vue de l'item
                //on redirige vers la page de l'item
                $tab['item']=$item;
                $tab['token']=$tokenModif;
                $vue = new VueCreateur($tab);
                $vue->render(10);
            }



    }

    public function traiterItemFormulaire($id) {
        $app = \Slim\Slim::getInstance();
        $item= item::find($id);
        $l = Liste::find($item['liste_id']);
        $tk = $l['tokenModif'];

        if(isset($_POST['valider_form']) && $_POST['valider_form']=='creationItem') {

            $nom = $app->request->post("nameItem");
            $description = $app->request->post("descrItem");
            $tarif = $app->request->post("tarif");
            $url = $app->request->post("url");

            //pour l'image
            if(isset($_POST["exampleRadios"])){
                $typeIm = $_POST["exampleRadios"];
            }

            //On nettoie les urls
            $nom = filter_var($nom, FILTER_SANITIZE_STRING);
            $description = filter_var($description, FILTER_SANITIZE_STRING);
            $tarif = filter_var($tarif, FILTER_SANITIZE_NUMBER_FLOAT);


            //NouveauItem
            $li = new Item();
            $li->nom = $nom;
            $li->descr = $description;
            $li->tarif = $tarif;
            $li->liste_id = -1;
            $li->url = $url;

            if(isset($_SESSION["idCompte"])){
                $li->idCreateur = $_SESSION["idCompte"];
            }

            //pour le type de l'image
            if($typeIm=="url"){
                $li->urlImage = 1;
            }
            $li->img = $_POST["image"];

            //sauvegarde dans la base
            $li->save();

            $id = $li->id;
            $tk = 0;


        }elseif($_POST["valider_form"]=="modificationItem"){

            $item= item::find($id);



            $nom = $app->request->post("nameItem");
            $description = $app->request->post("descrItem");
            $tarif = $app->request->post("tarif");
            $url = $app->request->post("url");


            //pour l'image
            if(isset($_POST["exampleRadios"])){
                $typeIm = $_POST["exampleRadios"];
            }

            //On nettoie les urls
            $nom = filter_var($nom, FILTER_SANITIZE_STRING);
            $description = filter_var($description, FILTER_SANITIZE_STRING);


            $item->nom=$nom;
            $item->descr=$description;
            $item->tarif=$tarif;
            $item->url=$url;
            //pour le type de l'image
            if($typeIm=="url"){
                $item->urlImage = 1;
            }
            $item->img = $_POST["image"];

            echo $description;


            $item->save();

        }

        //on redirige vers la page de l'item
        $url = $app->urlFor("item", ["token"=>$tk, "id"=>$id]);
        $app->redirect($url);

    }

    public function traiterReservationItem($tokenAcces, $idItem){
        //on récupère l'application
        $app = \Slim\Slim::getInstance();

        //on récupère l'item
        $item = Item::find($idItem);

        //on récupère la liste
        $liste = Liste::find($item["liste_id"]);

        //si le token est le bon
        if($tokenAcces == $liste['tokenAcces']){
            //on récupère les infos
            $nomParticipant = $app->request()->post("nomParticipant");
            $mess=$_POST["msg"];

            //on enregistre le nom dans une variable de session
            $_SESSION["nomParticipant"] = $nomParticipant;

            //on créé l'objet de réservation
            $reservation = new Reservation();
            $reservation->item_Id = $idItem;
            $reservation->nomParticipant = $nomParticipant;

            $compte = Compte::where("login", "=", $nomParticipant)->first();

            if($compte!= null){
                $reservation->id_participant =$compte->idCompte;
            }


            if(!empty($mess)){
                $m = new Message();
                $m->message=$mess;
                $m->item_id = $idItem;
                $m->save();
            }

            //on ajoute dans la base
            $reservation->save();

            //on met à jour l'objet item
            $item->reservation = true;
            $item->save();
        }

        //on redirige vers la page de l'item
        $url = $app->urlFor("item", ["token"=>$tokenAcces, "id"=>$idItem]);
        $app->redirect($url);
    }

    public function AjoutImageFormulaire() {
        $vue = new VueCreateur(null);
        $vue->render(11);
    }

    public function traiterAjoutImageFormulaire($idItem)
    {
        $app = \Slim\Slim::getInstance();

        if (isset($_POST['valider_form']) && $_POST['valider_form'] == 'valid_formulaireImg') {
            $target_dir = "img/";
            $target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
            $uploadOk = 1;
            $imageFileType = strtolower(pathinfo($target_file, PATHINFO_EXTENSION));
            $check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
            if ($check !== false) {
                $uploadOk = 1;
            } else {
                $uploadOk = 0;
            }

            if ($_FILES["fileToUpload"]["size"] > 500000) {
                $uploadOk = 0;
            }

            if ($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg") {
                $uploadOk = 0;
            }
            $image = $app->request->post("image");
            $item = Item::find($idItem);
            $item->img = $image;
            $item->save();

            if ($uploadOk != 0) {

                if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_dir . $item->nom . "." . $imageFileType)) {

                    $item->img = $item->nom . "." . $imageFileType;
                    $item->save();
                } else {
                    $item->delete();
                }
            }
        }
    }

    public function traiterSupressionImage($id) {
        if($id != null) {
            $item = Item::find($id);
                $item->img = null;
                $item->save();
        }
    }

    /**
     *
     */
    public function modifImageFormulaire($id){

        if($id != null){
            $item = Item::find($id);
            $tab['image']=$item->img;
            $vue = new VueCreateur($tab);
            $vue->render(11);


        }
    }

    public function traiterModifImage($id){
        $app = \Slim\Slim::getInstance();
        if(isset($_POST['valider_form']) && $_POST['valider_form']=='modificationImage') {
            $item = Item::find($id);
            $idl = $item['liste_id'];
            $l = Liste::find($idl);

            $tokenAcces = $l['tokenAcces'];
            $image = $app->request->post("image");
            $item->img = $image;
            $item->save();
        }

        if(empty($tokenAcces)){
            $tokenAcces = 0;
        }
       $url= $app->urlFor('item',['token'=>$tokenAcces, 'id'=>$id]);
        $app->redirect($url);
    }


    public function creerCagnotte($id, $token){
        $app = \Slim\Slim::getInstance();
        $item= item::find($id);
        $liste = Liste::where('no','=',$item['liste_id'])->first();
        if($id != null){
            if($liste['tokenModif']==$token){

                $tab['item']=$item;
                $tab['token']=$token;
                $tab['cagnotte']=false;
                $vue = new VueCreateur($tab);
                $vue->render(13);
            }else{
                $vue= new VueCreateur($item);
                $vue->render(10);
            }

    }}

    /**
     * @param $id
     * @param $token
     */
    public function traiterCreationCagnotte($id, $token){
        $app = \Slim\Slim::getInstance();
        $item= item::find($id);
        $liste = Liste::where('no','=',$item['liste_id'])->first();
        $cagnotte = Cagnotte::where('item_id','=',$id)->first();


       if(isset($cagnotte)){
           $tab['item']=$item;
           $tab['token']=$liste['tokenModif'];
           $tab['cagnotte']=true;

           $vue= new VueCreateur($tab);
           $vue->render(13);

       }else{
           $tab['item']=$item;
           $tab['token']=$liste['tokenModif'];
           $tab['cagnotte']=false;
           $cagnotte= new Cagnotte();
           $cagnotte->item_id=$id;
           $cagnotte->montant=0;
           $cagnotte->nom= filter_var($app->request->post('nomCagnotte'),FILTER_SANITIZE_STRING);
           $cagnotte->participant= '';
           $cagnotte->save();
           $vue= new VueCreateur($tab);
           $vue->render(13);

       }


    }

    public function participerCagnotte($token, $id){
        $item= item::find($id);
        $liste = Liste::where('no','=',$item['liste_id'])->first();
        $c = Cagnotte::where('item_id','=',$item['id'])->first();
        $montantActuel = $item['tarif']-$c['montant'];


        if($id!=null){
            if($token==$liste['tokenAcces']){
                if($item["reservation"]==false){
                    if($montantActuel>0){
                        $tab["item"]=$item;
                        $tab["token"]=$token;
                        $tab["cagnotte"]=$montantActuel;
                        $vue = new VueParticipant($tab);
                        $vue->render(10);
                    }
                }

            }

        }

    }

    public function traiterParticipationCagnotte($token, $id){
        $app = \Slim\Slim::getInstance();
        $item= item::find($id);
        $liste = Liste::where('no','=',$item['liste_id'])->first();
        $c = Cagnotte::where('item_id','=',$item['id'])->first();

        if($token == $liste['tokenAcces']){
            $montant = $app->request->post('montant');
            $montant= intval($montant);
            //$montant=filter_var($montant,FILTER_SANITIZE_STRING);

            $resCagnotte=$montant+$c['montant'];
            if($resCagnotte<=$item['tarif']){
                $c->montant=$resCagnotte;
                $c->save();
            }else{
                $tab['probleme']='erreur: Total de la cagnotte dépasse le tarif total !';
                $vue= new VueParticipant($tab);
                $vue->render(-2);
            }

            $tarif = $item['tarif']-$resCagnotte;



            $tab['item']=$item;
            $tab['token']=$token;
            $vue=new VueParticipant($tab);
            $vue->render(10);
        }




    }




}
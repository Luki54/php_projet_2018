<?php
/**
 * Created by PhpStorm.
 * User: gabrielsega
 * Date: 14/01/2019
 * Time: 18:55
 */

namespace wishlist\controleur;


use Slim\Slim;
use wishlist\modele\Compte;
use wishlist\modele\Item;
use wishlist\modele\Liste;
use wishlist\vue\VueParticipant;

class ReservationControleur {
    public function afficherReservations(){
        $app = Slim::getInstance();

        //on vérifie que la personne est bien connectée
        if(isset($_SESSION["idCompte"])){
            //on récupère le compte
            $compte = Compte::find($_SESSION["idCompte"]);

            //on récupère toutes ses participants
            $participations = $compte->aParticipe->toArray();

            //on vérifie qu'il y ait bien des participations
            if(!empty($participations)){
                //le tableau des valeurs
                foreach ($participations as $p){
                    $item = Item::find($p["item_id"]);

                    //on met l'item et la liste dans un tableau
                    $tabIL["item"] = $item;
                    $tabIL["liste"] = Liste::find($item["liste_id"]);

                    //qu'on met ensuite dans un tableau final
                    $tab[] = $tabIL;
                }
            }else{
                $tab = null;
            }




            $vue = new VueParticipant($tab);
            $vue->render(5);
        }
    }

}
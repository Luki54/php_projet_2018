<?php
/**
 * Created by PhpStorm.
 * User: gabrielsega
 * Date: 05/12/2018
 * Time: 10:23
 */

namespace wishlist\controleur;
use Slim\Slim;
use wishlist\modele\Item;
use wishlist\vue\VueCreateur;
use wishlist\vue\VueGeneral;
use wishlist\vue\VueParticipant;
use wishlist\modele\Liste;


class ListeControleur {
    public function getListeDetaille($idListe, $token){
        //on r�cup�re la liste
        $list = Liste::find($idListe);

        //on r�cup�re tous les items de la liste
        $objs = $list->item;

        //on r�cup�re tous les messages de la liste
        $mes = $list->message;

        //on va utiliser un tableau contenant : les infos de la liste
        $tab['liste'] = $list;
        //les items de la liste
        $tab['item'] =$objs->toArray();
        $tab['message'] =$mes->toArray();
        $tab['token'] = $token;
        $tab["probleme"] = "La liste n'est pas encore validée par son créateur";

        //on initialise la vue
        $vue = new VueParticipant($tab);

        //on v�rifie que la personne a le bon token
        switch ($token){
            case $list["tokenAcces"]:
                //si la liste est validée
                if($list->validee == true){
                    //on affiche la bonne vue
                    $vue->render(2);
                }else{
                    $vue->render(-2);
                }

                break;

            case $list["tokenModif"]:
                //on affiche la vue avec le bouton de modification et de partage
                $vue = new VueCreateur($tab);
                $vue->render(7);
                break;

            default:
                //on affiche une page d'erreur
                $vue->render(-1);
                break;
        }

    }
    
    public function getAllListes(){
    	$listes = Liste::get();
    	$vue=new VueParticipant($listes->toArray());
    	$vue->render(1);
    }

    public function afficherFormulaire($idListe, $tokenModif){
        //la liste et le token servent uniquement en cas de modification d'une liste
        //on récupère la liste
        if($idListe != null){
            $liste = Liste::find($idListe);

            //on vérifie le token
            if($tokenModif == $liste["tokenModif"]){
                $tab["liste"] = $liste;
                $tab["token"] = $tokenModif;
                $vue = new VueCreateur($tab);
            }else{
                $vue = new VueCreateur(null);
            }
        }else{
            $vue = new VueCreateur(null);
        }

        $vue->render(5);
    }

    public function traiterFormulaire($id){
        //on r�cup�re l'instance de slim
        $app = \Slim\Slim::getInstance();
        $token = "";


        //on v�rifie l'existence du formulaire
        if (isset($_POST["validerCreationListe"])) {
            //si c'est une création de liste
            if ($_POST["validerCreationListe"] == "creationListe") {
                //on r�cup�re les valeurs
                $titre = $app->request->post("titre");
                $descri = $app->request->post("description");
                $date = $app->request->post("date");

                //on nettoie les urls
                $titre = filter_var($titre, FILTER_SANITIZE_STRING);
                $descri = filter_var($descri, FILTER_SANITIZE_STRING);

                //on cr�� la nouvelle liste
                $li = new Liste();
                $li->titre = $titre;
                $li->description = $descri;
                $li->expiration = $date;

                //on cr�� les tokens d'acc�s et de modification
                $tokenAcces = random_bytes(32);
                $tokenAcces = bin2hex($tokenAcces);
                $tokenModif = random_bytes(32);
                $tokenModif = bin2hex($tokenModif);
                $li->tokenAcces = $tokenAcces;
                $li->tokenModif = $tokenModif;
                
                //on r�cup�re le token pour la redirection
                $token=$tokenModif;

                //si il est enregistré
                if(isset($_SESSION["idCompte"])){
                    $li->user_id = $_SESSION["idCompte"];
                }
              
                //on sauvegarde dans la base
                $li->save();

                //on regarde si un cookie existe déjà
                if(isset($_COOKIE[$li->no])){

                    //on détruit le cookie
                    unset($_COOKIE[$li->no]);

                    //pour la date de fin
                    $time = strtotime($date);

                    //on le remet
                    setcookie($li->no, $li->no, $time);
                }else{
                    //pour la date de fin
                    $time = strtotime($date);

                    //on le remet
                    setcookie($li->no, $li->no, $time);
                }


                //on r�cup�re l'id de la liste pour la redirection
                $id= $li->no;

                //on redirige
                $adresse = $app->urlFor("afficherListe", ["token"=>$token, "idListe"=>$id]);
                $app->redirect($adresse);
                
            } elseif ($_POST["validerCreationListe"] == "modificationListe") {

                //on récupère la la liste correspondante
                $li = Liste::find($id);

                //on r�cup�re les valeurs
                $titre = $app->request->post("titre");
                $descri = $app->request->post("description");
                $date = $app->request->post("date");

                //on nettoie les urls
                $titre = filter_var($titre, FILTER_SANITIZE_STRING);
                $descri = filter_var($descri, FILTER_SANITIZE_STRING);

                //on attribue les nouvelles valeurs
                $li->titre = $titre;
                $li->description = $descri;
                $li->expiration = $date;
                
                //on r�cup�re le token pour la redirection
                $token=$li->tokenModif;
                

                echo $descri;

                //on sauvegarde dans la base
                $li->save();
                
                //on r�cup�re l'id de la liste pour la redirection
                $id= $li->no;

                //on redirige
                $adresse = $app->urlFor("afficherListe", ["token"=>$token, "idListe"=>$id]);
                $app->redirect($adresse);
            }

        }elseif(isset($_POST["supprimerListe"])) {
            //on récupère la la liste correspondante
            $li = Liste::find($id);

            //on la supprime
            $li->delete();

            //on redirige
            $app->redirect($app->urlFor("accueil"));

        }elseif(isset($_POST["rendrePublic"])){
            //on récupère la la liste correspondante
            $li = Liste::find($id);

            //on la rend publique
            $li->public = true;

            //on enregistre
            $li->save();

            //on redirige
            $adresse = $app->urlFor("afficherListe", ["token"=>$li->tokenModif, "idListe"=>$id]);
            $app->redirect($adresse);
        }
    }
    
    public function urlPartage($numListe){
    	
    	//on r�cup�re le token d'acc�s de la liste
    	$list = Liste::find($numListe);
    	$token = $list->tokenAcces;
    	$tab['id']=$numListe;
    	$tab['token']=$token;
    	
    	//on initialise la vue
    	$vue = new VueCreateur($tab);
    	$vue->render(12);
    	
    }

    //pour afficher toutes les listes publiques
    public function afficherListesPubliques(){
        $d = date("Y-m-d");

        //on récupère toutes les listes publiques
        $donnes = Liste::where("public", "=", "1")
                    ->where("expiration", ">=", "$d")
                    ->where("validee", "=", "1")
                    ->orderBy("expiration", "ASC")
                    ->get();

        //c'est une vue générale
        $vue = new VueGeneral($donnes);

        //on affiche la vue
        $vue->render(2);
    }

    public function traiterRecherche(){
        //on vérifie l'existance du formulaire
        if(isset($_POST["recherche"])){
            //on récupère le nom
            $nom = $_POST["nom"];

            //on recherhe les listes ayant un user_id
            $listes = Liste::where("user_id", ">", "0")
                            ->orderBy("expiration", "ASC")
                            ->get();

            $temp = [];

            //on vérifie s'il y a un nom ou pas
            if(!empty($nom)){
                //on parctourt toutes les listes
                foreach ($listes as $liste){
                    //on recupère son créateur
                    $createur = $liste->createur;

                    //on regarde si son nom est comme ça
                    if(strpos($createur["login"], $nom) !== false){
                        //on ajoute la liste dans le tableau
                        $temp[] = $liste;
                    }
                }
            }else{
                //si il n'y a pas de nom on affiche tout
                $d = date("Y-m-d");
                $temp = Liste::where("public", "=", "1")
                    ->where("expiration", ">=", "$d")
                    ->where("validee", "=", "1")
                    ->orderBy("expiration", "ASC")
                    ->get();
            }




            //tableau avec les infos
            $tab["listes"] = $temp;
            $tab["nom"] = $nom;

            //c'est une vue générale
            $vue = new VueGeneral($tab);

            //on affiche la vue
            $vue->render(2);
        }
    }

    //pour joindre une liste
    public function joindreUneListe(){
        //on créé la vue et on l'affiche
        $vue = new VueCreateur(null);

        $vue->render(1);
    }

    public function traiterJoindreUneListe(){
        $app = Slim::getInstance();

        //on vérifie que qqn est connecté
        if(isset($_SESSION["type"])){
            //on vérifie qu'il y a bien tout ce qu'il faut
            if(isset($_POST["valider_form"])){
                //on récupère la liste
                $liste = Liste::find($_POST["numListe"]);

                if($liste != null){
                    if($liste->tokenModif == $_POST["token"] && $liste->user_id == null){
                        //on change le numéro de la liste
                        $liste->user_id = $_SESSION["idCompte"];
                        $liste->save();

                        //on rebalance sur la liste
                        $url = $app->urlFor("afficherListe", ["token"=> $liste->tokenModif, "idListe"=>$liste->no]);
                        $app->redirect($url);
                    }else{
                        $vue = new VueGeneral("il n'est pas possible d'ajouter cette liste, il y a eu un problème");
                        $vue->render(3);
                    }
                }else{
                    $vue = new VueGeneral("il n'est pas possible d'ajouter cette liste, il y a eu un problème");
                    $vue->render(3);
                }
            }else{
                $vue = new VueGeneral("il n'est pas possible d'ajouter cette liste, il y a eu un problème");
                $vue->render(3);
            }
        }else{
            $vue = new VueGeneral("il n'est pas possible d'ajouter cette liste, il y a eu un problème");
            $vue->render(3);
        }
    }

    //pour les listes que l'on rend publique
    public function traiterPublique($id){
        //on test si on vient du bon formulaire
        if(isset($_POST["public"])){
            //on récupère la liste
            $liste = Liste::find($id);
            $liste->public = "1";
            $liste->save();

            //on redirige
            $app = Slim::getInstance();
            $url = $app->urlFor("afficherListe", ["token"=>$liste->tokenModif, "idListe"=>$liste->no]);

            $app->redirect($url);
        }
    }

    //pour les listes que l'on rend publique
    public function traiterValide($id){
        //on test si on vient du bon formulaire
        if(isset($_POST["valide"])){
            //on récupère la liste
            $liste = Liste::find($id);
            $liste->validee = "1";
            $liste->save();

            //on redirige
            $app = Slim::getInstance();
            $url = $app->urlFor("afficherListe", ["token"=>$liste->tokenModif, "idListe"=>$liste->no]);

            $app->redirect($url);
        }
    }

    public function afficherAjoutItem($idListe){
        //on teste si il est connecté
        if(isset($_SESSION["idCompte"])){
            $tab["idListe"] = $idListe;
            $items = Item::where("idCreateur", "=", $_SESSION["idCompte"])
                ->where("liste_id", "<", "0")
                ->get()->toArray();

            $tab["items"] = $items;
            $vue = new VueCreateur($tab);
            $vue->render(3);
        }else{
            $vue = new VueGeneral("Il faut être connecté pour ajouter des items à une liste");
            $vue->render(3);
        }
    }

    public function traiterAjoutItem($idListe){
        if(isset($_POST["ajout"])){
            $idItem = $_POST["ajout"];
            $item = Item::find($idItem);
            $item->liste_id = $idListe;
            $item->save();

            $app = Slim::getInstance();
            $liste = Liste::find($idListe);
            $token = $liste->tokenModif;
            $url = $app->urlFor("afficherListe", ["token"=>$token, "idListe"=>$idListe]);
            $app->redirect($url);
        }
    }
}
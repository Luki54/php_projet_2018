<?php

namespace wishlist\controleur;
use \wishlist\vue\VueParticipant;
use \wishlist\modele\Message;
use \wishlist\modele\Liste;

class MessageControleur{
	
	public function getMessagesListe($idListe){
		$messages=Message::find($idListe);
		$vue=new VueParticipant($messages->toArray());
		$vue->render(9);
	}
	
	public function afficherFormulaire($idListe){
		
		//on teste si la liste existe bien
		if($idListe != null){
			$liste = Liste::find($idListe);
			$tab["liste"] = $liste;
			$vue = new VueParticipant($tab);
		}else{
			$vue = new VueParticipant(null);
		}
		$vue->render(8);
	}
	
	public function traiterMessageFormulaire($idListe) {
		$app = \Slim\Slim::getInstance();
		if(isset($_POST['valider_form']) && $_POST['valider_form']=='valid_formulaireMessage') {
			
			$message = $app->request->post("Message");
			Echo $message;
			//On nettoie les urls
			$message = filter_var($message, FILTER_SANITIZE_STRING);
			
			
			//Nouveau message
			$m = new Message();
			$m->liste_id =$idListe ;
			$m->message = $message;

			//sauvegarde dans la base
			$m->save();

			//redirection
            $liste = Liste::find($idListe);
			
			$this->getMessagesListe($idListe);
		
			
			//on redirige la vers une autre route
			$app->redirect($app->urlFor("afficherListe", ["token"=>$liste->tokenAcces, "idListe"=>$idListe]));
			
		}
	}
}
<?php
/**
 * Created by PhpStorm.
 * User: gabrielsega
 * Date: 12/01/2019
 * Time: 15:28
 */

namespace wishlist\controleur;


use wishlist\modele\Compte;
use wishlist\modele\Liste;
use wishlist\vue\VueGeneral;

class GeneralControleur{

    public function afficherAccueil(){
        //on créé la vue
        $vue = new VueGeneral(null);
        //on l'affiche
        $vue->render(1);
    }

    public function afficherCreateurs(){
        //on récupère tous les comptes de créateurs
        $createurs = Compte::where("type", "=", "createur")->get();

        //tableau pour l'ensemble des créateurs
        $tab = [];

        //on parcourt tous les créateurs
        foreach($createurs as $crea){
            //on récupère ses listes
            $listes = Liste::where("user_id", "=", $crea->idCompte)
                            ->where("validee", "=", "1")
                            ->where("public", "=", "1")
                            ->get();

            //on regarde si ce n'est vide
            if(!empty($listes)){
                $tab[] = $crea;
            }
        }

        //on créé la vue
        $vue = new VueGeneral($tab);
        $vue->render(4);
    }
}
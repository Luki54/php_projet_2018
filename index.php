<?php
/**
 * File:  index.php
 * Creation Date: 04/12/2017
 * description:
 *
 * @author: canals
 */

require_once('vendor/autoload.php');

use wishlist\modele\Item as Item;
use wishlist\modele\Liste as Liste;
use Illuminate\Database\Capsule\Manager as DB;
use wishlist\controleur\ListeControleur;
use \wishlist\controleur\ItemControleur;
use \wishlist\controleur\MessageControleur;
use \wishlist\controleur\CompteControleur;
use \wishlist\controleur\GeneralControleur;
use \wishlist\controleur\ReservationControleur;

//d�marrage de slim
$app = new \Slim\Slim();

//on initialise eloquent
$ini = parse_ini_file('src/conf/conf.ini');
$db = new DB();
$db->addConnection($ini);
$db->setAsGlobal();
$db->bootEloquent();
//la session
session_start();

$app->get('/listes/afficherTout', function (){
    $liscontrol=new ListeControleur();
    $liscontrol->getAllListes();
})->name("toutesLesListes");

$app->get('/liste/:token/:idListe', function ($token, $idListe){
    $lc = new ListeControleur();
    $lc->getListeDetaille($idListe, $token);
})->name("afficherListe");

$app->get('/item/:token/:id', function($token, $id) {
    $controlItem = new ItemControleur();
    $controlItem->getUnItem($id, $token);
})->name("item");

$app->post('/item/:token/:id', function($token, $id) {
    $controlItem = new ItemControleur();
    $controlItem->getUnItem($id, $token);
    $controlItem->traiterItemFormulaire(null);
})->name("reserverItem");


$app->get('/Liste/affichageItems',function(){
    $itecontrol=new ItemControleur();
    $itecontrol->getAllItems();
});


//cr�ation d'une liste
$app->get('/creerListe',function (){
    $liscontrol = new ListeControleur();
    //Si on créé une nouvelle liste alors il n'y a pas d'id ni de token
    $liscontrol->afficherFormulaire(null, null);
})->name("creationListe");

//traitement des donn�es de la liste
$app->post('/creerListe',function (){
    $liscontrol = new ListeControleur();
    $liscontrol->traiterFormulaire(null);
});

//cr�ation d'un nouvel item
$app->get('/creerItem', function(){
    $conti = new ItemControleur();
    $conti->creationItem();
})->name("creerItem");

$app->post('/creerItem', function(){
    $conti = new ItemControleur();
    $conti->traiterItemFormulaire(null);
});

//pour modifier une liste
$app->get('/modifierListe/:tokenModif/:numListe', function ($tokenModif, $numListe){
    $controleur = new ListeControleur();
    $controleur->afficherFormulaire($numListe, $tokenModif);
})->name("modifierListe");

//pour traiter sa modification
$app->post('/modifierListe/:tokenModif/:numListe', function($tokenModif, $numListe){
    $controleur = new ListeControleur();
    $controleur->traiterFormulaire($numListe);
});

//pour modifier un item
$app->get('/modifItem/:tokenModif/:idItem',function ($tokenModif,$idItem){
    $controleur = new ItemControleur();
    $controleur->modifItem($tokenModif,$idItem);
});

//pour modifier un item
$app->post('/modifItem/:tokenModif/:idItem',function ($tokenModif,$idItem){
    $controleur = new ItemControleur();
    $controleur->traiterItemFormulaire($idItem);
})->name('modifItem');

//pour ajouter un message � une liste
$app->get('/ajouterMessage/:numListe',function($numListe){
    $control=new MessageControleur();
    $control->afficherFormulaire($numListe);
})->name("ajouterMessage");

$app->post('/ajouterMessage/:numListe',function ($numListe){
    $control = new MessageControleur();
    $control->traiterMessageFormulaire($numListe);
});

//pour la suppression d'un item
$app->post('/supprimerItem/:tokenModif/:idItem', function ($tokenModif, $idItem){
    $control = new ItemControleur();
    $control->traiterSuppressionItem($tokenModif, $idItem);
})->name("suppressionItem");

//pour l'ajout d'une réservation à un item
$app->post('/reservationItem/:tokenAcces/:idItem', function ($tokenAcces, $idItem){
    $control = new ItemControleur();
    $control->traiterReservationItem($tokenAcces, $idItem);
})->name("traiterReservation");


//ajout d'une image à un item
$app->post('/ajouterImage/:idItem', function($idItem){
    $control = new ItemControleur();
    $control->traiterAjoutImageFormulaire($idItem);
});

$app->get('/ajouterImage/:idItem', function(){
    $control = new ItemControleur();
    $control->ajoutImageFormulaire();
})->name('ajouterImage');


//modifier l'image d'un item
$app->get('/modifierImage/:idItem', function($idItem){
    $control = new ItemControleur();
    $control->modifImageFormulaire($idItem);
})->name('modifierImage');

$app->post('/modifierImage/:idItem', function($idItem){
    $control = new ItemControleur();
    $control->traiterModifImage($idItem);
});

//pour la création d'un compte
$app->get('/creerCompte', function (){
    $control = new CompteControleur();
    $control->formulaireCreation();
})->name("creationCompte");

//pour la création d'un compte
$app->post('/creerCompte', function (){
    $control = new CompteControleur();
    $control->traiterFormulaireCreation();

});

//Supression d'une image
$app->get('/supprimerImage/:idItem', function ($idItem) {
    $control = new ItemControleur();
    $control->traiterSupressionImage($idItem);
});

//pour la connexion
$app->get('/connexion', function (){
    $control = new CompteControleur();

    //si personne n'est connecté
    if(!isset($_SESSION["login"])){
        $control->formulaireConnexion();
    }else{
        $control->formulaireDeconnexion();
    }

})->name("connexion");

//pour la connexion
$app->post('/connexion', function (){
    $control = new CompteControleur();

    if(isset($_SESSION["login"])){
        $control->traiterFormulaireDeConnexion();
    }else{
        $control->traiterFormulaireConnexion();
    }

});

//affichage de l'url de partage
$app->post('/partager/:numListe', function($numListe){
	$controleur = new ListeControleur();
	$controleur->urlPartage($numListe);
})->name("partager");


//pour modifier un compte
$app->get('/modifier', function (){
    $controleur = new CompteControleur();
    $controleur->afficherModif();
})->name("afficherModif");

$app->post('/modifier', function (){
    $controleur = new CompteControleur();
    $controleur->traiterModif();
});

//pour voir ses participations
$app->get('/participations', function (){
    $controleur = new ReservationControleur();
    $controleur->afficherReservations();
})->name("participations");

//pour les recherches
$app->get('/rechercher', function (){
    $control = new ListeControleur();
    $control->afficherListesPubliques();
})->name('rechercher');

$app->post('/rechercher', function (){
    $control = new ListeControleur();
    $control->traiterRecherche();
});


//pour joindre une liste
$app->get('/joindreUneListe', function (){
    $control = new ListeControleur();
    $control->joindreUneListe();
})->name("joindreUneListe");

$app->post('/joindreUneListe', function (){
    $control = new ListeControleur();
    $control->traiterJoindreUneListe();
});

//pour rendre une liste publique
$app->post('/rendrePublique/:id', function ($id){
    $control = new ListeControleur();
    $control->traiterPublique($id);
})->name("rendrePublique");

//pour valider une liste
$app->post('/rendreValide/:id', function ($id){
    $control = new ListeControleur();
    $control->traiterValide($id);
})->name("rendreValide");


$app->get('/creerCagnotte/:token/:idItem',function ($token, $idItem){
    $control = new ItemControleur();
    $control->creerCagnotte($idItem,$token);

})->name('creerCagnotte');

$app->post('/creerCagnotte/:token/:idItem',function ($token, $idItem){
    $control = new ItemControleur();
    $control->traiterCreationCagnotte($idItem,$token);

});



$app->get('/participerCagnotte/:token/:idItem',function ($token, $idItem){
    $control = new ItemControleur();
    $control->participerCagnotte($token,$idItem);

});

$app->post('/participerCagnotte/:token/:idItem',function ($token, $idItem){
    $control = new ItemControleur();
    $control->traiterParticipationCagnotte($token,$idItem);

})->name('participerCagnotte');


$app->get("/ajouterItem/:idListe", function ($idListe){
    $control = new ListeControleur();
    $control->afficherAjoutItem($idListe);
})->name("afficherAjoutItem");

$app->post("/ajouterItem/:idListe", function ($idListe){
    $control = new ListeControleur();
    $control->traiterAjoutItem($idListe);
});

//explication token
/*
 * cr�er une route d'�dition qui contient token �dition
 * cr�er une route de partage qui contient le token
 * partage : utiliser la m�me + l'id pour acc�der � un item
 * edit : idem pour pouvoir modifier un item
 * penser � faire des v�rifications
 */

//pour l'accueil
$app->get('/accueil', function (){
    $controleur = new GeneralControleur();
    $controleur->afficherAccueil();
})->name("accueil");

//pour l'affichage des créateurs
$app->get('/afficherCreateurs', function (){
    $control = new GeneralControleur();
    $control->afficherCreateurs();
})->name("createurs");

$app->get("/", function (){
    $app = \Slim\Slim::getInstance();
    $app->redirect($app->urlFor("accueil"));
});


$app->run();